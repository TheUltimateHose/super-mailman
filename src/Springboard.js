var Springboard = function(x, y, force, state) {
    var sb = game.add.sprite(x + 35, y + 70, R.image.springboard);
    sb.force = force || -400;

    sb.animations.add(R.anim.active, [1, 0], 2);

    game.physics.p2.enable(sb);
    sb.body.static = true;
    sb.body.height = 35;

    sb.body.setCollisionGroup(state.coll_objects);
    sb.body.collides([state.coll_objects, state.coll_player]);

    sb.anchor.setTo(.5, 1);

    sb.sfx = game.add.audio(R.audio.spring);

    sb.body.onBeginContact.add(function(other, _1, _2, _3, _4) {
        sb.animations.play(R.anim.active);
        sb.sfx.play();

        AddMedals(other.x, other.y, 1);

        other.velocity.x *= 2;
        other.velocity.y = sb.force;
    });

    return sb;
};