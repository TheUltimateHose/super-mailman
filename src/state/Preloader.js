var Preloader = function() {};
Preloader.prototype = {
    init: function() {
        game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        game.stage.disableVisibilityChange = true;
        game.input.mspointer.stop();
        game.plugins.add(PhaserNineSlice.Plugin);

        Number.prototype.clamp = function(min, max) {
            return this <= min ? min : this >= max ? max : this;
        };
    },
    preload: function() {
        console.info("Starting preloading...");

        game.load.bitmapFont(R.font.mario, DATA_URLS["font_png"], DATA_URLS["font_xml"]);

        game.load.image(R.image.title, DATA_URLS[R.image.title]);

        game.load.spritesheet(R.image.player, DATA_URLS[R.image.player], 72, 97);
        game.load.spritesheet(R.image.player2, DATA_URLS[R.image.player2], 72, 97);

        game.load.image(R.image.checkbox.ticked, DATA_URLS[R.image.checkbox.ticked]);
        game.load.image(R.image.checkbox.unticked, DATA_URLS[R.image.checkbox.unticked]);

        game.load.nineSlice(R.image.slider.bar, DATA_URLS[R.image.slider.bar], 0);
        game.load.image(R.image.slider.end, DATA_URLS[R.image.slider.end]);
        game.load.image(R.image.slider.pin, DATA_URLS[R.image.slider.pin]);

        game.load.spritesheet(R.image.springboard, DATA_URLS[R.image.springboard], 70, 70);
        game.load.spritesheet(R.image.flag_red, DATA_URLS[R.image.flag_red], 70, 70);
        game.load.spritesheet(R.image.switch, DATA_URLS[R.image.switch], 70, 70);
        game.load.spritesheet(R.image.laser, DATA_URLS[R.image.laser], 70, 70);
        game.load.spritesheet(R.image.laser_beam, DATA_URLS[R.image.laser_beam], 70, 70);
        game.load.spritesheet(R.image.barrier, DATA_URLS[R.image.barrier], 70, 70);
        game.load.spritesheet(R.image.medals, DATA_URLS[R.image.medals], 44, 77);

        game.load.tilemap(R.map.start, DATA_URLS[R.map.start], null, Phaser.Tilemap.TILED_JSON);
        game.load.image(R.image.tileset_base, DATA_URLS[R.image.tileset_base]);

        game.load.image(R.image.blank, DATA_URLS[R.image.blank]);

        game.load.image(R.image.box, DATA_URLS[R.image.box]);
        game.load.image(R.image.box_empty, DATA_URLS[R.image.box_empty]);
        game.load.image(R.image.box_warning, DATA_URLS[R.image.box_warning]);

        game.load.image(R.image.fireball, DATA_URLS[R.image.fireball]);

        game.load.audio(R.audio.jump, DATA_URLS[R.audio.jump]);
        game.load.audio(R.audio.lose, DATA_URLS[R.audio.lose]);
        game.load.audio(R.audio.spring, DATA_URLS[R.audio.spring]);
        game.load.audio(R.audio.win, DATA_URLS[R.audio.win]);
        game.load.audio(R.audio.laser.main, DATA_URLS[R.audio.laser._1]);
        game.load.audio(R.audio.medals, DATA_URLS[R.audio.medals]);

        console.info("Done preloading.");
    },
    create: function() {
        game.state.start(R.state.mainmenu);
    }
};
