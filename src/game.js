var game = null;
var lastMapInfo = {};
var Medals = [];

function init() {
    game = new Phaser.Game(1280, 720, Phaser.Canvas, '', null, false, false);

    game.state.add(R.state.preload, Preloader);
    game.state.add(R.state.mainmenu, MainMenu);
    game.state.add(R.state.maingame, MainGame);
    game.state.add(R.state.win, Win);
    game.state.add(R.state.gameover, GameOver);

    game.state.start(R.state.preload);
}

function level(lvl) {
    lastMapInfo.next_map = "map_" + lvl;
    game.state.start(R.state.maingame);
}